﻿using Dapper;
using log4net;
using Newtonsoft.Json;
using SatsportNewsService.Model;
using SatsportNewsService.ResponseModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SatsportNewsService.Service
{
    public class SsNewsService
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string BASE_URL = "https://central.satsport247.com";
        private Setting GlobalSetting { get; set; }

        internal IDbConnection GetConnection()
        {
            IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DataEntities"].ToString());
            db.Open();
            return db;
        }

        public async Task Run()
        {
            await GetAccessToken();
        }

        private async Task GetAccessToken()
        {
            try
            {
                using (var db = GetConnection())
                {
                    // Get setting for agentcode and secretkey from table
                    GlobalSetting = db.Query<Setting>("SELECT * FROM tblGlobalSettings").FirstOrDefault();
                    if (GlobalSetting == null)
                    {
                        logger.Error("Please first configure your system.");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(GlobalSetting.AccessToken) ||
                        !GlobalSetting.TokenUpdateTime.HasValue ||
                        GlobalSetting.TokenUpdateTime.Value.AddDays(1).AddHours(-3) < DateTimeOffset.UtcNow)
                    {
                        object data = new
                        {
                            agentcode = GlobalSetting.AgentCode,
                            secretkey = GlobalSetting.SecretKey
                        };

                        var tokenResponse = GetResponse<TokenResponse>("/api/get_access_token", HttpMethod.Post, data);
                        if (tokenResponse != null)
                        {
                            if (!string.IsNullOrWhiteSpace(tokenResponse.Token))
                            {
                                GlobalSetting.AccessToken = tokenResponse.Token;
                                string query = "UPDATE tblGlobalSettings SET IsTokenUsed = 1, AccessToken = @AccessToken, TokenUpdatedTime = @TokenUpdatedTime WHERE AgentCode = @AgentCode";
                                await db.ExecuteAsync(query, new { GlobalSetting.AccessToken, TokenUpdatedTime = DateTimeOffset.UtcNow, GlobalSetting.AgentCode });
                            }
                            else if (!tokenResponse.Success)
                            {
                                logger.Error(tokenResponse.Message);
                                return;
                            }
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(GlobalSetting.AccessToken))
                {
                    await UpdateSportList();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unable to get access token", ex);
            }
        }

        private async Task UpdateSportList()
        {
            try
            {
                List<Sport> sportsEntityData = new List<Sport>();
                if (!GlobalSetting.SportLastSyncTime.HasValue
                    || GlobalSetting.SportLastSyncTime.Value.AddMinutes(GlobalSetting.SportSyncInterval) <= DateTimeOffset.UtcNow)
                {
                    var data = new
                    {
                        access_token = GlobalSetting.AccessToken
                    };

                    var sports = GetResponse<List<ResponseData>>("/api/get_sport_list", HttpMethod.Post, data);
                    if (sports != null)
                    {
                        // Add or Update sport list in table
                        using (var db = GetConnection())
                        {
                            string query = "SELECT SportTypeID, SportName FROM tblSport WHERE SportTypeID IN @Ids";
                            var sportsEntity = db.Query<Sport>(query, new { Ids = sports.Select(o => o.EventType.Id).ToArray() }).ToList();
                            foreach (var sport in sports)
                            {
                                var sportEntity = sportsEntity.FirstOrDefault(o => o.SportTypeID == sport.EventType.Id);
                                if (sportEntity == null)
                                {
                                    query = string.Format("INSERT INTO tblSport (SportTypeID, SportName, IsActive, IsDelete, CreatedBy, CreatedOn) VALUES('{0}', '{1}', 1, 0, {2}, @CreatedOn)", sport.EventType.Id, sport.EventType.Name, 1);
                                    await db.ExecuteAsync(query, new { CreatedOn = DateTimeOffset.UtcNow });
                                    sportsEntityData.Add(new Sport()
                                    {
                                        SportTypeID = sport.EventType.Id,
                                        SportName = sport.EventType.Name
                                    });
                                }
                                else
                                {
                                    query = string.Format("UPDATE tblSport SET SportName = '{0}', ModifiedBy = {1}, ModifiedOn = @ModifiedOn WHERE SportTypeID = '{2}'", sport.EventType.Name, 1, sport.EventType.Id);
                                    await db.ExecuteAsync(query, new { ModifiedOn = DateTimeOffset.UtcNow });
                                    sportsEntityData.Add(sportEntity);
                                }
                            }

                            // Change sport last sync date time
                            query = "UPDATE tblGlobalSettings SET SportLastSyncTime = @SportLastSyncTime WHERE AgentCode = @AgentCode";
                            await db.ExecuteAsync(query, new { SportLastSyncTime = DateTimeOffset.UtcNow, GlobalSetting.AgentCode });
                        }
                    }
                }
                else
                {
                    using (var db = GetConnection())
                    {
                        string query = "SELECT SportTypeID, SportName FROM tblSport WHERE SportName = 'Cricket'";
                        sportsEntityData = db.Query<Sport>(query).ToList();
                    }
                }

                if (sportsEntityData != null)
                {
                    foreach (var sportEntityData in sportsEntityData)
                    {
                        // Right now we are update only cricket data only
                        if (string.Equals(sportEntityData.SportName, "Cricket", StringComparison.InvariantCultureIgnoreCase))
                        {
                            await UpdateTournamentList(sportEntityData.SportTypeID);
                            await UpdateNews(sportEntityData.SportTypeID, sportEntityData.SportName);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unable to update sport list", ex);
            }
        }

        private async Task UpdateTournamentList(string sportId)
        {
            try
            {
                List<Tournament> tournamentsEntityData = new List<Tournament>();
                if (!GlobalSetting.TournamentLastSyncTime.HasValue
                    || GlobalSetting.TournamentLastSyncTime.Value.AddMinutes(GlobalSetting.TournamentSyncInterval) <= DateTimeOffset.UtcNow)
                {
                    var data = new
                    {
                        access_token = GlobalSetting.AccessToken,
                        strEventTypeId = sportId
                    };
                    var tournaments = GetResponse<List<ResponseData>>("/api/get_tournament_list", HttpMethod.Post, data);
                    if (tournaments != null)
                    {
                        // Add or Update tournament list in table
                        using (var db = GetConnection())
                        {
                            string query = "SELECT TournamentUID FROM tblTournament WHERE TournamentUID IN @Ids AND SportTypeID = @sportId";
                            var tournamentsEntity = db.Query<Tournament>(query, new { Ids = tournaments.Select(o => o.Competition.Id).ToArray(), sportId }).ToList();
                            foreach (var tournament in tournaments)
                            {
                                var tournamentEntity = tournamentsEntity.FirstOrDefault(o => o.TournamentUID == tournament.Competition.Id);
                                if (tournamentEntity == null)
                                {
                                    query = "INSERT INTO tblTournament (TournamentUID, SportTypeID, TournamentName, IsActive, IsDelete, CreatedBy, CreatedOn) VALUES(@Id, @sportId, @Name, 1, 0, @CreatedBy, @CreatedOn)";
                                    var queryData = new
                                    {
                                        tournament.Competition.Id,
                                        sportId,
                                        tournament.Competition.Name,
                                        CreatedBy = 1,
                                        CreatedOn = DateTimeOffset.UtcNow
                                    };
                                    await db.ExecuteAsync(query, queryData);
                                    tournamentsEntityData.Add(new Tournament()
                                    {
                                        TournamentUID = tournament.Competition.Id
                                    });
                                }
                                else
                                {
                                    query = "UPDATE tblTournament SET TournamentName = @Name, ModifiedBy = @ModifiedBy, ModifiedOn = @ModifiedOn WHERE TournamentUID = @TournamentUID";
                                    var queryData = new
                                    {
                                        tournament.Competition.Name,
                                        ModifiedBy = 1,
                                        ModifiedOn = DateTimeOffset.UtcNow,
                                        TournamentUID = tournament.Competition.Id
                                    };
                                    await db.ExecuteAsync(query, queryData);
                                    tournamentsEntityData.Add(tournamentEntity);
                                }
                            }

                            // Change tournament last sync date time
                            query = "UPDATE tblGlobalSettings SET TournamentLastSyncTime = @TournamentLastSyncTime WHERE AgentCode = @AgentCode";
                            await db.ExecuteAsync(query, new { TournamentLastSyncTime = DateTimeOffset.UtcNow, GlobalSetting.AgentCode });
                        }
                    }
                }
                else
                {
                    using (var db = GetConnection())
                    {
                        string query = "SELECT TournamentUID FROM tblTournament";
                        tournamentsEntityData = db.Query<Tournament>(query).ToList();
                    }
                }

                foreach (var tournamentEntityData in tournamentsEntityData)
                {
                    await UpdateMatchList(sportId, tournamentEntityData.TournamentUID);
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Unable to update tournament list, SportId: {0}", sportId), ex);
            }
        }

        private async Task UpdateMatchList(string sportId, string tournamentId)
        {
            try
            {
                if (!GlobalSetting.MatchLastSyncTime.HasValue
                    || GlobalSetting.MatchLastSyncTime.Value.AddMinutes(GlobalSetting.MatchSyncInterval) <= DateTimeOffset.UtcNow)
                {
                    var data = new
                    {
                        access_token = GlobalSetting.AccessToken,
                        strEventTypeId = sportId,
                        strCompetitionId = tournamentId
                    };
                    var matchs = GetResponse<List<ResponseData>>("/api/get_match_list", HttpMethod.Post, data);
                    if (matchs != null)
                    {
                        // Add or Update match list in table
                        using (var db = GetConnection())
                        {
                            string query = "SELECT MatchUID FROM tblMatches WHERE MatchUID IN @Ids";
                            var matchesEntity = db.Query<Match>(query, new { Ids = matchs.Select(o => o.Event.Id).ToArray() }).ToList();
                            foreach (var match in matchs)
                            {
                                var matchEntity = matchesEntity.FirstOrDefault(o => o.MatchUID == match.Event.Id);
                                if (matchEntity == null)
                                {
                                    query = string.Format("INSERT INTO tblMatches (MatchUID, TournamentUID, SportTypeID, MatchName, MatchCountryCode, MatchTimeZone, MatchVenue, MatchOpenDate, IsActive, IsDelete, CreatedBy, CreatedOn)"
                                        + " VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', 1, 0, {8}, @CreatedOn)", match.Event.Id, tournamentId, sportId, match.Event.Name, match.Event.CountryCode, match.Event.Timezone, match.Event.Venue, match.Event.OpenDate, 1);
                                    await db.ExecuteAsync(query, new { CreatedOn = DateTimeOffset.UtcNow });
                                }
                                else
                                {
                                    query = string.Format("UPDATE tblMatches SET MatchName = '{0}', MatchCountryCode = '{1}', MatchTimeZone = '{2}', MatchVenue = '{3}', MatchOpenDate = '{4}', ModifiedBy = {5}, ModifiedOn = @ModifiedOn WHERE MatchUID = '{6}'",
                                        match.Event.Name, match.Event.CountryCode, match.Event.Timezone, match.Event.Venue, match.Event.OpenDate, 1, match.Event.Id);
                                    await db.ExecuteAsync(query, new { ModifiedOn = DateTimeOffset.UtcNow });
                                }
                            }

                            // Change match last sync date time
                            query = "UPDATE tblGlobalSettings SET MatchLastSyncTime = @MatchLastSyncTime WHERE AgentCode = @AgentCode";
                            await db.ExecuteAsync(query, new { MatchLastSyncTime = DateTimeOffset.UtcNow, GlobalSetting.AgentCode });

                            foreach (var match in matchs)
                            {
                                await UpdateMatchScoreUrl(match.Event.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Unable to update match list, TournamentId: {0}", tournamentId), ex);
            }
        }

        private async Task UpdateMatchScoreUrl(string matchId)
        {
            try
            {
                var data = new
                {
                    access_token = GlobalSetting.AccessToken,
                    match_id = matchId,
                    domain_name = BASE_URL
                };
                var widget = GetResponse<Widget>("/api/get_scoreurl_by_centralid", HttpMethod.Post, data);
                if (widget != null
                    && string.Equals(widget.Status, "success", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Add live score widget url in table
                    using (var db = GetConnection())
                    {
                        string query = string.Format("SELECT * FROM tblLiveScore WHERE MatchUID = '{0}'", matchId);
                        var liveScoreEntity = db.Query<LiveScore>(query).FirstOrDefault();
                        if (liveScoreEntity == null)
                        {
                            query = string.Format("INSERT INTO tblLiveScore (MatchUID, ScoreURL, IsActive, IsDelete, CreatedBy, CreatedOn) VALUES('{0}', '{1}', 1, 0, {2}, @CreatedOn)", matchId, widget.Data.ScoreUrl, 1);
                            await db.ExecuteAsync(query, new { CreatedOn = DateTimeOffset.UtcNow });
                        }
                        else
                        {
                            query = string.Format("UPDATE tblLiveScore SET ScoreURL = '{0}', ModifiedBy = {1}, ModifiedOn = @ModifiedOn WHERE MatchUID = '{2}'", widget.Data.ScoreUrl, 1, matchId);
                            await db.ExecuteAsync(query, new { ModifiedOn = DateTimeOffset.UtcNow });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Unable to update match score url, Match Id: {0}", matchId), ex);
            }
        }

        private async Task UpdateNews(string sportId, string sportCategory)
        {
            try
            {
                if (!GlobalSetting.NewsLastSyncTime.HasValue
                    || GlobalSetting.NewsLastSyncTime.Value.AddMinutes(GlobalSetting.NewsSyncInterval) <= DateTimeOffset.UtcNow)
                {
                    var data = new
                    {
                        access_token = GlobalSetting.AccessToken,
                        category = sportCategory
                    };

                    var newsResponse = GetResponse<NewsResponse>("/livescore/news/list", HttpMethod.Post, data);
                    if (newsResponse != null)
                    {
                        // Add news list in table
                        using (var db = GetConnection())
                        {
                            string query = "SELECT NewsUID FROM tblNews WHERE NewsUID IN @Ids";
                            var newsEntity = db.Query<Model.News>(query, new { Ids = newsResponse.Data.News.Select(o => o.Id).ToArray() }).ToList();
                            foreach (var news in newsResponse.Data.News)
                            {
                                if (!newsEntity.Any(o => o.NewsUID == news.Id.ToString()))
                                {
                                    var content = news.Content.Replace("'", "''");
                                    var title = news.Title.Replace("'", "''");
                                    var shortDescription = news.ShortDescription.Replace("'", "''");
                                    query = string.Format("INSERT INTO tblNews (NewsUID, SportTypeID, NewsTitle, NewsExcerpt, NewsDescription, NewsDate, NewsThumbnail, NewsFeaturedImage, NewsIsFeatured, NewsIsTrending, IsActive, IsDelete, CreatedBy, CreatedOn) "
                                        + "VALUES('{0}', '{1}', '{2}', '{3}', '{4}', @NewsDate, '{5}', '{6}', {7}, {8}, 1, 0, {9}, @CreatedOn)", news.Id, sportId, title, shortDescription, content, news.Thumbnail, news.Image, 0, 0, 1);
                                    await db.ExecuteAsync(query, new { CreatedOn = DateTimeOffset.UtcNow, NewsDate = DateTimeOffset.FromUnixTimeSeconds(news.PublishedDate) });
                                }
                            }

                            // Change news last sync date time
                            query = "UPDATE tblGlobalSettings SET NewsLastSyncTime = @NewsLastSyncTime WHERE AgentCode = @AgentCode";
                            await db.ExecuteAsync(query, new { NewsLastSyncTime = DateTimeOffset.UtcNow, GlobalSetting.AgentCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Unable to update news, sport Id: {0}", sportId), ex);
            }
        }

        private T GetResponse<T>(string url, HttpMethod httpMethod, object data)
        {
            try
            {
                string requestUrl = string.Format("{0}{1}", BASE_URL, url);
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
                httpWebRequest.Method = httpMethod.ToString();
                if (data != null)
                {
                    string parameter = JsonConvert.SerializeObject(data, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    httpWebRequest.ContentType = "application/json";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(parameter);
                    }
                }

                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            string rawData = reader.ReadToEnd();
                            var responseData = JsonConvert.DeserializeObject<T>(rawData);
                            return responseData;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Request: {0}", url), ex);
            }

            return default;
        }

        private string SaveImage(string url)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;
                WebResponse webResponse = webRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                Image image = Image.FromStream(stream);
                string fileId = Guid.NewGuid().ToString().Replace("-", "");
                string fileName = Path.Combine("~/NewsImage", fileId);
                image.Save(fileName);
                stream.Flush();
                stream.Close();
                webResponse.Close();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Unable to save image, Url: {0}", url), ex);
            }

            return string.Empty;
        }
    }
}
