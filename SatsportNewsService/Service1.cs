﻿using log4net;
using SatsportNewsService.Service;
using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Timers;

namespace SatsportNewsService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Timer Timer { get; } = new Timer();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Timer.Elapsed += OnElapsedTime;
                Timer.Interval = 300000; //number in miliseconds  
                Timer.Enabled = true;
                logger.Debug("Service started");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                Timer.AutoReset = false;
                Timer.Enabled = false;
                logger.Debug("Service stopped");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            SsNewsService service = new SsNewsService();
            service.Run().Wait();
        }
    }
}
