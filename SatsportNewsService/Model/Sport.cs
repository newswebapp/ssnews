﻿namespace SatsportNewsService.Model
{
    public class Sport
    {
        public int SportID { get; set; }
        public string SportTypeID { get; set; }
        public string SportName { get; set; }
    }
}
