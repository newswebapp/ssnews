﻿namespace SatsportNewsService.Model
{
    public class LiveScore
    {
        public int LiveScoreID { get; set; }
        public string MatchUID { get; set; }
        public string ScoreURL { get; set; }
    }
}
