﻿using System;

namespace SatsportNewsService.Model
{
    class Match
    {
        public int MatchID { get; set; }
        public string MatchUID { get; set; }
        public string SportTypeID { get; set; }
        public string TournamentUID { get; set; }
        public string MatchName { get; set; }
        public string MatchCountryCode { get; set; }
        public string MatchTimeZone { get; set; }
        public string MatchVenue { get; set; }
        public DateTime MatchOpenDate { get; set; }
    }
}
