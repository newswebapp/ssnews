﻿using System;

namespace SatsportNewsService.Model
{
    public class News
    {
        public int NewsID { get; set; }
        public string NewsUID { get; set; }
        public string SportTypeID { get; set; }
        public string NewsTitle { get; set; }
        public string NewsExcerpt { get; set; }
        public string NewsDescription { get; set; }
        public DateTimeOffset NewsDate { get; set; }
        public string NewsThumbnail { get; set; }
        public string NewsFeaturedImage { get; set; }
        public bool NewsIsFeatured { get; set; }
        public bool NewsIsTrending { get; set; }
    }
}
