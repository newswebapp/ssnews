﻿namespace SatsportNewsService.Model
{
    public class Tournament
    {
        public int TournamentID { get; set; }
        public string SportTypeID { get; set; }
        public string TournamentUID { get; set; }
        public string TournamentName { get; set; }
    }
}
