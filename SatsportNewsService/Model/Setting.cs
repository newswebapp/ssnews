﻿using System;

namespace SatsportNewsService.Model
{
    public class Setting
    {
        public string AgentCode { get; set; }
        public string SecretKey { get; set; }
        public string AccessToken { get; set; }
        public bool? IsTokenUsed { get; set; }
        public DateTimeOffset? TokenUpdateTime { get; set; }
        public DateTimeOffset? SportLastSyncTime { get; set; }
        public int SportSyncInterval { get; set; }
        public DateTimeOffset? TournamentLastSyncTime { get; set; }
        public int TournamentSyncInterval { get; set; }
        public DateTimeOffset? MatchLastSyncTime { get; set; }
        public int MatchSyncInterval { get; set; }
        public DateTimeOffset? NewsLastSyncTime { get; set; }
        public int NewsSyncInterval { get; set; }
    }
}
