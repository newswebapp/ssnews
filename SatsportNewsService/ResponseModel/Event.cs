﻿using System;

namespace SatsportNewsService.ResponseModel
{
    public class Event : CommonField
    {
        public string CountryCode { get; set; }
        public string Timezone { get; set; }
        public string Venue { get; set; }
        public DateTime OpenDate { get; set; }
    }
}
