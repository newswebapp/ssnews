﻿using Newtonsoft.Json;

namespace SatsportNewsService.ResponseModel
{
    public class Widget
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("data")]
        public WidgetData Data { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class WidgetData
    {
        [JsonProperty("scoreUrl")]
        public string ScoreUrl { get; set; }
    }
}
