﻿using Newtonsoft.Json;

namespace SatsportNewsService.ResponseModel
{
    public class TokenResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("agentcode")]
        public string AgentCode { get; set; }
        
        [JsonProperty("success")]
        public bool Success { get; set; }
        
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
