﻿namespace SatsportNewsService.ResponseModel
{
    public class CommonField
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
