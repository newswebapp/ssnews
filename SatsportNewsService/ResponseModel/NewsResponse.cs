﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SatsportNewsService.ResponseModel
{
    public class NewsResponse
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        
        [JsonProperty("data")]
        public NewsData Data { get; set; }
    }

    public class NewsData
    {
        [JsonProperty("bld")]
        public DateTime Bld { get; set; }

        [JsonProperty("arts")]
        public List<News> News { get; set; }
        
        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public class News
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("pid")]
        public int PId { get; set; }
        
        [JsonProperty("seo")]
        public string SEO { get; set; }
        
        [JsonProperty("tit")]
        public string Title { get; set; }
        
        [JsonProperty("des")]
        public string ShortDescription { get; set; }
        
        [JsonProperty("con")]
        public string Content { get; set; }
        
        [JsonProperty("pub")]
        public long PublishedDate { get; set; }
        
        [JsonProperty("tst")]
        public string Tst { get; set; }
        
        [JsonProperty("thb")]
        public string Thumbnail { get; set; }
        
        [JsonProperty("img")]
        public string Image { get; set; }
        
        [JsonProperty("cap")]
        public string Cap { get; set; }
        
        [JsonProperty("aut")]
        public string Aut { get; set; }
        
        [JsonProperty("ava")]
        public string Ava { get; set; }
    }
}
