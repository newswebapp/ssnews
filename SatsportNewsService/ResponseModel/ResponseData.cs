﻿namespace SatsportNewsService.ResponseModel
{
    public class ResponseData
    {
        public CommonField EventType { get; set; }
        public CommonField Competition { get; set; }
        public Event Event { get; set; }
        public int MarketCount { get; set; }
    }
}
