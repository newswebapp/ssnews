﻿using System.Data.Common;

namespace SatsportNewsAdmin.Services
{
    public interface IContextService
    {
        DbConnection GetDbconnection();
    }
}
