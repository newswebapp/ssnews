﻿using SatsportNewsAdmin.Entity;

namespace SatsportNewsAdmin.Services
{
    public interface IUserService
    {
        User Login(User user);
    }
}
