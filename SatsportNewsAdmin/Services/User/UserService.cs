﻿using Dapper;
using Microsoft.Extensions.Logging;
using SatsportNewsAdmin.Entity;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SatsportNewsAdmin.Services
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> logger;
        private readonly IContextService contextService;

        public UserService(ILogger<UserService> logger,
            IContextService contextService)
        {
            this.logger = logger;
            this.contextService = contextService;
        }

        public User Login(User user)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(user.Username)
                    || string.IsNullOrWhiteSpace(user.Password))
                {
                    return null;
                }

                using var db = contextService.GetDbconnection();
                string query = @"SELECT u.Username
                                    FROM tblUser AS u
                                    WHERE u.IsActive = 1 AND u.IsDelete = 0 AND u.Username = @Username AND u.Password = @Password";
                var userEntity = db.Query<User>(query, new { user.Username, Password = ComputeSha256Hash(user.Password) }).FirstOrDefault();
                return userEntity;
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to login", ex);
                return null;
            }
        }

        private string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
