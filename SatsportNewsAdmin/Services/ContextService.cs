﻿using Microsoft.Extensions.Configuration;
using System.Data.Common;
using System.Data.SqlClient;

namespace SatsportNewsAdmin.Services
{
    public class ContextService : IContextService
    {
        private readonly IConfiguration config;

        public ContextService(IConfiguration config)
        {
            this.config = config;
        }

        public DbConnection GetDbconnection()
        {
            return new SqlConnection(config.GetConnectionString("DefaultConnection"));
        }
    }
}
