﻿using System;

namespace SatsportNewsAdmin.Entity
{
    public class User
    {
        public int UserID { get; set; }

        public string Username { get; set; }

        public string UserFullname { get; set; }

        public int Role { get; set; }

        public string EmailID { get; set; }

        public string PhoneNo { get; set; }

        public string Password { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
