﻿using Microsoft.EntityFrameworkCore;

namespace SatsportNewsAdmin.DataContext
{
    public class AppContext : DbContext
    {
        public AppContext() { }
        public AppContext(DbContextOptions<AppContext> options) : base(options) { }
    }
}
