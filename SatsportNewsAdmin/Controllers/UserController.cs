﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SatsportNewsAdmin.Models;
using SatsportNewsAdmin.Services;

namespace SatsportNewsAdmin.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Index()
        {
            return View("~/Views/Login/Index.cshtml");
        }

        [HttpPost]
        public IActionResult Login(UserModel model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
