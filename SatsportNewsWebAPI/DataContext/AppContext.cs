﻿using Microsoft.EntityFrameworkCore;

namespace SatsportNewsWebAPI.DataContext
{
    public class AppContext : DbContext
    {
        public AppContext() { }
        public AppContext(DbContextOptions<AppContext> options) : base(options) { }
    }
}
