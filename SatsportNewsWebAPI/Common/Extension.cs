﻿using System;

namespace SatsportNewsWebAPI.Common
{
    public static class Extension
    {
        public static bool TryParseDateTime(this object datetimeString, out DateTime dateTime)
        {
            return DateTime.TryParse(datetimeString.ToString(), out dateTime);
        }

        public static bool TryParseDateTime(this object datetimeString, out DateTime? dateTime)
        {
            var result = DateTime.TryParse(datetimeString.ToString(), out DateTime date);
            dateTime = date;
            return result;
        }

        public static bool TryParseGuid(this object guidString, out Guid guid)
        {
            return Guid.TryParse(guidString.ToString(), out guid);
        }

        public static bool TryParseBoolean(this object boolString, out bool value)
        {
            return bool.TryParse(boolString.ToString(), out value);
        }
    }
}
