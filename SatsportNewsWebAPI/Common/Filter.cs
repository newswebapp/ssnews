﻿using System;
using System.Collections.Generic;

namespace SatsportNewsWebAPI.Common
{
    public class Filter
    {
        public Filter()
        {
            PageIndex = 0;
            PageSize = 10;
            SortBy = new Dictionary<string, string>();
            Parameters = new Dictionary<string, object>();
        }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public Dictionary<string, string> SortBy { get; set; }
        public Dictionary<string, object> Parameters { get; set; }

        public bool TryGetValue<T>(string key, out T result)
        {
            result = default;
            if (!Parameters.ContainsKey(key) || Parameters[key] == null)
            {
                return false;
            }

            var type = typeof(T);
            if (type == typeof(DateTime))
            {
                if (Parameters[key].TryParseDateTime(out DateTime resultTemp))
                {
                    result = (T)Convert.ChangeType(resultTemp, type);
                    return true;
                }
            }
            else if (type == typeof(DateTime?))
            {
                if (Parameters[key].TryParseDateTime(out DateTime? resultTemp))
                {
                    result = (T)Convert.ChangeType(resultTemp, typeof(DateTime));
                    return true;
                }
            }
            else if (type == typeof(Guid))
            {
                if (Parameters[key].TryParseGuid(out Guid value))
                {
                    result = (T)Convert.ChangeType(value, type);
                    return true;
                }
            }
            else if (type == typeof(bool))
            {
                if (Parameters[key].TryParseBoolean(out bool value))
                {
                    result = (T)Convert.ChangeType(value, type);
                    return true;
                }
            }
            else if (type == typeof(int))
            {
                if (int.TryParse(Parameters[key].ToString(), out int value))
                {
                    result = (T)Convert.ChangeType(value, type);
                    return true;
                }
            }
            else if (type == typeof(string))
            {
                var value = Parameters[key].ToString().Trim();
                result = (T)Convert.ChangeType(value, type);
                return true;
            }
            else if (type == typeof(Guid?))
            {
                if (Parameters[key].TryParseGuid(out Guid value))
                {
                    result = (T)Convert.ChangeType(value, typeof(Guid));
                    return true;
                }
            }

            return false;
        }
    }
}
