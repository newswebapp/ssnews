﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SatsportNewsWebAPI.Common;
using SatsportNewsWebAPI.Model;
using SatsportNewsWebAPI.Service;
using System;
using System.Linq;

namespace SatsportNewsWebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommonController : ControllerBase
    {
        private readonly ILogger<CommonController> logger;
        private readonly ICommonService commonService;

        public CommonController(ILogger<CommonController> logger,
            ICommonService commonService)
        {
            this.logger = logger;
            this.commonService = commonService;
        }

        [HttpPost("GetSliderList")]
        public IActionResult GetSliderList()
        {
            ResponseModel model = new();
            try
            {
                var sliders = commonService.GetSliderList();
                if (sliders != null)
                {
                    model.Data = sliders.Select(o => new SliderModel()
                    {
                        Id = o.SliderID,
                        Description = o.SliderDescription,
                        Title = o.SliderTitle
                    }).ToList();

                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider list", ex);
            }

            model.SetErrorMessage("Unable to get slider list");
            return Ok(model);
        }

        [HttpGet("GetSliderImage")]
        public IActionResult GetSliderImage(int id)
        {
            try
            {
                return Ok(commonService.GetSliderImage(id));
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider image", ex);
            }

            return NotFound();
        }

        [HttpGet("GetSliderLayer")]
        public IActionResult GetSliderLayer(int id)
        {
            try
            {
                return Ok(commonService.GetSliderLayer(id));
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider layer", ex);
            }

            return NotFound();
        }

        [HttpPost("GetMatchList")]
        public IActionResult GetMatchList([FromBody] Filter filter)
        {
            ResponsePageDataModel model = new();
            try
            {
                filter ??= new Filter();
                var matches = commonService.GetMatchList(filter);
                if (matches != null)
                {
                    model.Data = matches.Select(o => new MatchModel()
                    {
                        Id = o.MatchID,
                        Name = o.MatchName,
                        CountryCode = o.MatchCountryCode,
                        Venue = o.MatchVenue,
                        TimeZone = o.MatchTimeZone,
                        OpenDate = o.MatchOpenDate,
                        Tournament = new TournamentModel()
                        {
                            Id = o.Tournament.TournamentID,
                            Name = o.Tournament.TournamentName
                        }
                    }).ToList();

                    model.PageIndex = filter.PageIndex;
                    model.PageSize = filter.PageSize;
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get match list", ex);
            }

            model.SetErrorMessage("Unable to get match list");
            return Ok(model);
        }

        [HttpPost("GetNewsList")]
        public IActionResult GetNewsList([FromBody] Filter filter)
        {
            ResponsePageDataModel model = new();
            try
            {
                filter ??= new Filter();
                var news = commonService.GetNewsList(filter);
                if (news != null)
                {
                    model.Data = news.Select(o => new NewsModel()
                    {
                        Id = o.NewsID,
                        Title = o.NewsTitle,
                        Excerpt = o.NewsExcerpt,
                        Description = o.NewsDescription,
                        Date = o.NewsDate,
                        FeaturedImage = o.NewsFeaturedImage,
                        Thumbnail = o.NewsThumbnail,
                        IsFeatured = o.NewsIsFeatured,
                        IsTrending = o.NewsIsTrending
                    }).ToList();

                    model.PageIndex = filter.PageIndex;
                    model.PageSize = filter.PageSize;
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get news list", ex);
            }

            model.SetErrorMessage("Unable to get news list");
            return Ok(model);
        }
    
        [HttpGet("SaveImage")]
        public IActionResult SaveImage()
        {
            commonService.SaveImage();
            return Ok();
        }
    }
}
