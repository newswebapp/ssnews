﻿using System;

namespace SatsportNewsWebAPI.Model
{
    public class MatchModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CountryCode { get; set; }

        public string TimeZone { get; set; }

        public string Venue { get; set; }

        public DateTimeOffset OpenDate { get; set; }

        public TournamentModel Tournament { get; set; }
    }
}
