﻿using System.Collections.Generic;

namespace SatsportNewsWebAPI.Model
{
    public class ResponseModel
    {
        public object Data { get; set; }
        public List<string> ErrorMessages { get; set; }
        public bool HasError
        {
            get
            {
                return ErrorMessages?.Count > 0;
            }
        }

        public void SetErrorMessage(string message)
        {
            if (ErrorMessages == null)
            {
                ErrorMessages = new List<string>();
            }

            ErrorMessages.Add(message);
        }
    }
}
