﻿namespace SatsportNewsWebAPI.Model
{
    public class TournamentModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
