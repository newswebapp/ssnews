﻿using System;

namespace SatsportNewsWebAPI.Model
{
    public class NewsModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Excerpt { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public string Thumbnail { get; set; }

        public string FeaturedImage { get; set; }

        public bool IsFeatured { get; set; }

        public bool IsTrending { get; set; }
    }
}
