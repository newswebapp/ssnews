﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class LiveScore
    {
        public int LiveScoreID { get; set; }

        public string MatchUID { get; set; }

        public string ScoreURL { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
