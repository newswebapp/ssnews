﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class Sport
    {
        public int SportID { get; set; }
        public string SportTypeID { get; set; }
        public string SportName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
