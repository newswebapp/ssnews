﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class News
    {
        public int NewsID { get; set; }

        public string NewsUID { get; set; }

        public string SportTypeID { get; set; }

        public string NewsTitle { get; set; }

        public string NewsExcerpt { get; set; }

        public string NewsDescription { get; set; }

        public DateTime NewsDate { get; set; }

        public string NewsThumbnail { get; set; }

        public string NewsFeaturedImage { get; set; }

        public bool NewsIsFeatured { get; set; }

        public bool NewsIsTrending { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
