﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class Match
    {
        public int MatchID { get; set; }

        public string MatchUID { get; set; }

        public string SportTypeID { get; set; }

        public string TournamentUID { get; set; }

        public string MatchName { get; set; }

        public string MatchCountryCode { get; set; }

        public string MatchTimeZone { get; set; }

        public string MatchVenue { get; set; }

        public DateTimeOffset MatchOpenDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTimeOffset? ModifiedOn { get; set; }

        public Tournament Tournament { get; set; }
    }
}
