﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class Slider
    {
        public int SliderID { get; set; }

        public string SliderTitle { get; set; }

        public string SliderDescription { get; set; }

        public byte[] SliderImage { get; set; }

        public byte[] SliderLayer { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
