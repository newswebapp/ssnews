﻿using System;

namespace SatsportNewsWebAPI.Entity
{
    public class Tournament
    {
        public int TournamentID { get; set; }

        public string SportTypeID { get; set; }

        public string TournamentUID { get; set; }

        public string TournamentName { get; set; }

        public bool IsActive { get; set; }

        public bool IsDelete { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
