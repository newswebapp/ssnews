﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data.Common;

namespace SatsportNewsWebAPI.Service
{
    public class ContextService : IContextService
    {
        private readonly IConfiguration config;

        public ContextService(IConfiguration config)
        {
            this.config = config;
        }

        public DbConnection GetDbconnection()
        {
            return new SqlConnection(config.GetConnectionString("DefaultConnection"));
        }
    }
}
