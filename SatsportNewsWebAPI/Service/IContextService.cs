﻿using System.Data.Common;

namespace SatsportNewsWebAPI.Service
{
    public interface IContextService
    {
        DbConnection GetDbconnection();
    }
}
