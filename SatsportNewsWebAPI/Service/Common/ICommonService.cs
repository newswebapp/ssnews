﻿using SatsportNewsWebAPI.Common;
using SatsportNewsWebAPI.Entity;
using System.Collections.Generic;

namespace SatsportNewsWebAPI.Service
{
    public interface ICommonService
    {
        List<Slider> GetSliderList();
        string GetSliderImage(int id);
        string GetSliderLayer(int id);
        List<Match> GetMatchList(Filter filter);
        List<News> GetNewsList(Filter filter);
        string SaveImage();
    }
}
