﻿using Dapper;
using Microsoft.Extensions.Logging;
using SatsportNewsWebAPI.Common;
using SatsportNewsWebAPI.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;

namespace SatsportNewsWebAPI.Service
{
    public class CommonService : ICommonService
    {
        private readonly ILogger<CommonService> logger;
        private readonly IContextService contextService;

        public CommonService(ILogger<CommonService> logger,
            IContextService contextService)
        {
            this.logger = logger;
            this.contextService = contextService;
        }

        public List<Slider> GetSliderList()
        {
            try
            {
                using var db = contextService.GetDbconnection();
                string query = @"SELECT s.SliderID, s.SliderTitle, s.SliderDescription
                                    FROM tblSlider AS s
                                    WHERE s.IsActive = 1 AND s.IsDelete = 0";
                var sliders = db.Query<Slider>(query).ToList();
                return sliders;
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider list", ex);
                return null;
            }
        }

        public string GetSliderImage(int id)
        {
            try
            {
                using var db = contextService.GetDbconnection();
                string query = @"SELECT s.SliderImage
                                    FROM tblSlider AS s
                                    WHERE s.IsActive = 1 AND s.IsDelete = 0 AND s.SliderID = @id";
                var slider = db.Query<Slider>(query, new { id }).FirstOrDefault();
                if (slider != null)
                {
                    return Convert.ToBase64String(slider.SliderImage);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider image, id: {@id}", id, ex);
            }

            return null;
        }

        public string GetSliderLayer(int id)
        {
            try
            {
                using var db = contextService.GetDbconnection();
                string query = @"SELECT s.SliderLayer
                                    FROM tblSlider AS s
                                    WHERE s.IsActive = 1 AND s.IsDelete = 0 AND s.SliderID = @id";
                var slider = db.Query<Slider>(query, new { id }).FirstOrDefault();
                if (slider != null)
                {
                    return Convert.ToBase64String(slider.SliderLayer);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get slider layer, id: {@id}", id, ex);
            }

            return null;
        }

        public List<Match> GetMatchList(Filter filter)
        {
            try
            {
                filter ??= new Filter();
                using var db = contextService.GetDbconnection();
                string query = @"SELECT m.MatchID, m.TournamentUID, m.MatchName, m.MatchCountryCode,
                                    m.MatchTimeZone, m.MatchVenue, m.MatchOpenDate,
                                    t.TournamentUID, t.TournamentID, t.TournamentName
                                    FROM tblMatches AS m
                                    LEFT JOIN tblTournament AS t ON m.TournamentUID = t.TournamentUID
                                    WHERE m.IsActive = 1 AND m.IsDelete = 0";

                if (filter.Parameters.Any())
                {
                    if (filter.TryGetValue<DateTime>("startDate", out DateTime startDate))
                    {
                        query += string.Format(" AND m.MatchOpenDate >= '{0}'", startDate.ToUniversalTime().ToString("yyyy-MM-dd"));
                    }
                }

                if (filter.SortBy.Any())
                {
                    query += GetSortingQuery(filter.SortBy, GetMatchModelEntityMaping());
                }
                else
                {
                    query += " ORDER BY m.MatchOpenDate DESC";
                }

                query += GetPagingQuery(filter.PageIndex, filter.PageSize);
                var matchs = db.Query<Match, Tournament, Match>(query, (match, tournament) =>
                {
                    match.Tournament = tournament;
                    return match;
                },
                splitOn: "TournamentUID").ToList();
                return matchs;
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get match list", ex);
                return null;
            }
        }

        public List<News> GetNewsList(Filter filter)
        {
            try
            {
                filter ??= new Filter();
                using var db = contextService.GetDbconnection();
                string query = @"SELECT n.NewsID, n.NewsTitle, n.NewsExcerpt, n.NewsDescription,
                                    n.NewsDate, n.NewsThumbnail, n.NewsFeaturedImage, n.NewsIsFeatured, n.NewsIsTrending
                                    FROM tblNews AS n
                                    WHERE n.IsActive = 1 AND n.IsDelete = 0";
                if (filter.Parameters.Any())
                {
                    if (filter.TryGetValue("type", out string type))
                    {
                        switch (type.ToLower())
                        {
                            case "trending":
                                query += " AND n.NewsIsTrending = 1";
                                break;

                            case "featured":
                                query += " AND n.NewsIsFeatured = 1";
                                break;
                        }
                    }
                }

                if (filter.SortBy.Any())
                {
                    query += GetSortingQuery(filter.SortBy, GetNewsModelEntityMaping());
                }
                else
                {
                    query += " ORDER BY n.NewsDate DESC";
                }

                query += GetPagingQuery(filter.PageIndex, filter.PageSize);
                var news = db.Query<News>(query).ToList();
                return news;
            }
            catch (Exception ex)
            {
                logger.LogError("Unable to get news list", ex);
                return null;
            }
        }

        public string SaveImage()
        {
            try
            {
                string url = "https://news.livescore.com/newsapi/04/soccer/image/koke-and-sarabia-snap-back-at-van-der-vaart-with-2010-world-cup-jibe-7-1mbbw2qskgcu611riv7bh4yphz.jpg";
                string fileName = Path.GetFileName(url);
                string path = Path.Combine("NewsImage", fileName);
                WebClient webClient = new();
                //webClient.DownloadFile(new Uri(url), path);

                Stream stream = webClient.OpenRead(url);
                Bitmap bitmap = new Bitmap(stream);

                if (bitmap != null)
                {
                    bitmap.Save("Image1.png", ImageFormat.Png);
                }

                stream.Flush();
                stream.Close();
                webClient.Dispose();




                ////HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                ////webRequest.AllowWriteStreamBuffering = true;
                ////webRequest.Timeout = 30000;
                ////WebResponse webResponse = webRequest.GetResponse();
                ////Stream stream = webResponse.GetResponseStream();
                //////byte[] imageBytes;
                //////using (BinaryReader br = new(stream))
                //////{
                //////    imageBytes = br.ReadBytes(500000);
                //////    br.Close();
                //////}

                ////DirectoryInfo info = new DirectoryInfo("NewsImage");
                ////if (!info.Exists)
                ////{
                ////    info.Create();
                ////}

                ////string fileId = Guid.NewGuid().ToString().Replace("-", "");
                ////if (webResponse.ContentType == "image/jpeg")
                ////{
                ////    fileId += ".jpg";
                ////}
                ////else if (webResponse.ContentType == "image/png")
                ////{
                ////    fileId += ".png";
                ////}

                ////string fileName = Path.Combine("NewsImage", fileId);
                ////using (FileStream outputFileStream = new FileStream(fileName, FileMode.Create))
                ////{
                ////    stream.CopyTo(outputFileStream);
                ////}

                ////stream.Flush();
                ////stream.Close();

                //////var imgBase64String = Convert.ToBase64String(imageBytes);
                //////imgBase64String = string.Format("data:{0};base64,{1}", webResponse.ContentType, imgBase64String);
                //////MemoryStream ms = new MemoryStream(Convert.FromBase64String(imgBase64String));
                //////Image image = Image.FromStream(ms);
                //////ImageConverter converter = new();
                //////Image image = (Image)converter.ConvertFrom(imageBytes);
                //////image.Save(fileName);
                ////webResponse.Close();
            }
            catch (Exception ex)
            {
                //logger.Error(string.Format("Unable to save image, Url: {0}", url), ex);
            }

            return string.Empty;
        }

        private string GetPagingQuery(int PageIndex, int PageSize)
        {
            return string.Format(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY;", PageSize * PageIndex, PageSize);
        }

        private string GetSortingQuery(Dictionary<string, string> sortBy, Dictionary<string, string> mapProperty)
        {
            string query = string.Empty;
            foreach (var sort in sortBy)
            {
                if (mapProperty.ContainsKey(sort.Key))
                {
                    query += (!string.IsNullOrWhiteSpace(query) ? ", " : "") + mapProperty[sort.Key];
                    switch (sort.Value)
                    {
                        case "desc":
                            query += " DESC";
                            break;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(query))
            {
                query = " ORDER BY " + query;
            }

            return query;
        }

        private Dictionary<string, string> GetMatchModelEntityMaping()
        {
            return new Dictionary<string, string>()
            {
                { "name", "m.MatchName" },
                { "openDate", "m.MatchOpenDate" }
            };
        }

        private Dictionary<string, string> GetNewsModelEntityMaping()
        {
            return new Dictionary<string, string>()
            {
                { "date", "n.NewsDate" }
            };
        }
    }
}
